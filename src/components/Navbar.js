import { Link, NavLink, useNavigate } from "react-router-dom";
import CartIcon from "../assets/icons/cart.svg";
import styled from "styled-components";
import CartContext from "../Context/Cart/CartContext";
import { useContext } from "react";
import { FaShopify } from "react-icons/fa";
import { HiShoppingBag } from "react-icons/hi";

const Navbar = () => {
  const { cartItems } = useContext(CartContext);
  const navigate = useNavigate();
  const handleClick = () => {
    localStorage.removeItem("userToken");
    navigate("/");
  };

  return (
    <Nav>
      <NavContainer>
        <Left>
          <Link to={"/allproducts"}>
            <FaShopify />
            hopify
          </Link>
        </Left>
        <Mid>
          <Link to={"/allproducts/men's clothing"}>clothing</Link>
        </Mid>
        <Mid>
          <Link to={"/allproducts/jewelery"}>jewelery</Link>
        </Mid>
        <Mid>
          <Link to={"/allproducts/electronics"}>electronics</Link>
        </Mid>
        <Right>
          <NavRightContainer>
            <NavList>
              <NavItem>
                <NavLink to="/allproducts">
                  <HiShoppingBag />
                  Store
                </NavLink>
              </NavItem>

              <NavItem>
                <Link to="/cart">
                  <p>Cart</p>
                  <NavCartItem>
                    <img src={CartIcon} alt="Shopping cart" />

                    {cartItems.length > 0 && (
                      <CartCircle>{cartItems.length}</CartCircle>
                    )}
                  </NavCartItem>
                </Link>
              </NavItem>
              <LagoutButton onClick={handleClick}>logout</LagoutButton>
            </NavList>
          </NavRightContainer>
        </Right>
      </NavContainer>
    </Nav>
  );
};

//Styled Components
const Nav = styled.nav`
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  height: 4.5rem;
  font-size: 1em;
  z-index: 50;
  background-color: #eee;
  @media (max-width: 768px) {
    font-size: 0.85rem;
    border: none;
  }
`;

const NavContainer = styled.div`
  width: 100%;
  height: 100%;
  max-width: 1250px;
  margin: 0 auto;
  padding: 0 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 768px) {
    padding: 0 1rem;
    border-bottom: 1.5px solid #cfcfd0;
  }
  @media (max-width: 500px) {
    background-color: #eee;
  }
`;

const NavRightContainer = styled.div`
  @media (max-width: 500px) {
    width: 100%;
    position: fixed;
    top: calc(4.5rem - 100vh);
    left: 0;
    background-color: #fff;
    z-index: -1;
    transition: transform 600ms cubic-bezier(1, 0, 0, 1) 0ms;
  }
`;

const Left = styled.div`
  a {
    color: #13122e;
    font-weight: 700;
    font-size: 3rem;
    font-family: "Cormorant Garamond", serif;
    text-decoration: none;
  }
`;

const Mid = styled.div`
  a {
    color: #13122e;
    font-weight: 700;
    font-size: 2rem;
    font-family: "Cormorant Garamond", serif;
    text-decoration: none;
  }
`;
const Right = styled.div`
  font-family: "Work Sans", sans-serif;
`;

const NavList = styled.ul`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  list-style: none;

  @media (max-width: 500px) {
    flex-direction: column;
    flex-wrap: nowrap;
  }
`;

const NavItem = styled.li`
  margin: 0 1.25em;

  a,
  span {
    text-decoration: none;
    color: #13122e;
    display: flex;
    font-size: 1.5rem;
    justify-content: center;
    align-items: center;
    @media (max-width: 1.125vw) {
      width: 100%;
      align-items: center;
      justify-content: flex-start;
      padding: 1rem;
      border-bottom: 1px solid #cfcfd0;
      max-height: 2.813vh;
      &:hover {
        background-color: #cfcfd0;
      }
    }
    img {
      width: 1.6rem;
    }
  }
  p {
    display: none;
  }
  @media (max-width: 1.125vw) {
    width: 100%;
    &:nth-of-type(4) a {
      display: flex;
      justify-content: space-between !important;
      align-items: center;
      border: none;
    }
    p {
      display: initial;
    }
  }
`;

const NavCartItem = styled.div`
  position: relative;
  img {
    @media (max-width: 31.25vw) {
      display: none;
    }
  }
`;

const LagoutButton = styled.button`
  cursor: pointer;
  background-color: #dc143c;
  color: white;
  font-weight: 900;
  border: none;
  width: 3vw;
  height: 3.5vh;
  border-radius: 25vw;
  :hover {
    background-color: green;
    color: black;
    border: none;
  }
`;

const CartCircle = styled.div`
  position: absolute;
  top: -5px;
  right: -10px;
  background-color: #13122e;
  width: 1.125vw;
  height: 2.125vh;
  border-radius: 50%;
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 0.85em;
  @media (max-width: 500px) {
    position: initial;
  }
`;

export default Navbar;
