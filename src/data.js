import one from "./assets/img/1.png"
import two from "./assets/img/2.png"
import three from "./assets/img/3.png"
import four from "./assets/img/4.png"
import five from "./assets/img/5.png"
import six from "./assets/img/6.png"
import seven from "./assets/img/7.png"
import eight from "./assets/img/8.png"
import nine from "./assets/img/9.png"
import ten from "./assets/img/10.png"

export const products = [
    {
      id: 1,
      name: "Cerveza Modelo",
      price: 919.11,
      image: one,
    },
    {
      id: 2,
      name: "Diesel Life",
      price: 1257.92,
      image: two,
    },
    {
      id: 3,
      name: "Indian Cricket Team jersey",
      price: 1500.85,
      image: three,
    },
    {
      id: 4,
      name: "One Punch man - OK",
      price: 1250.9,
      image: four,
    },
    {
      id: 5,
      name: "Hiking jacket",
      price: 1750.85,
      image: five,
    },
    {
      id: 6,
      name: "Real Heart",
      price: 3100.61,
      image: six,
    },
    {
      id: 7,
      name: "Fredd - Black and White",
      price: 1801.1,
      image: seven,
    },
    {
      id: 8,
      name: "Star Wars - The Last",
      price: 1199.99,
      image: eight,
    },
    {
      id: 9,
      name: "Yellow Blouse",
      price: 2395.16,
      image: nine,
    },
    {
      id: 10, 
      name: "Rick and Morty - Supreme",
      price: 1243.82,
      image: ten,
    },
    {
      id: 10,
      name: "Rick and Morty - Supreme",
      price: 1243.82,
      image: ten,
    },
    {
      id: 10,
      name: "Rick and Morty - Supreme",
      price: 1243.82,
      image: ten,
    },
  ];