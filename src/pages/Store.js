// import { products } from "../data";
import { useEffect, useState } from "react";
import styled from "styled-components";
import ProductCard from "../components/ProductCard";
import axios from "axios";
import Navbar from "../components/Navbar";
import { useParams } from "react-router-dom";

const Store = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const { category } = useParams();

  // url: `/products${
  //   router?.query?.name ? `/category/${router.query.name}` : "/"
  // }`,

  useEffect(() => {
    setLoading(true);
    axios({
      method: "GET",
      url: `https://fakestoreapi.com/products/category/${category}`,
      // url: "https://api.escuelajs.co/api/v1/products",
    })
      .then((res) => {
        // console.log(res.data, "returned data");
        setProducts(res?.data);
      })
      .catch((e) => console.log(e))
      .finally(() => setLoading(false));
  }, [category]);

  return (
    <>
      <Navbar />
      {loading && (
        <div>
          {" "}
          <h1>Loading...</h1>
        </div>
      )}
      <Heading>
        <h1>Browse the Store!</h1>
        <p>New Arrivals for you! Check out our selection of products.</p>
      </Heading>
      <ProductsContainer>
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </ProductsContainer>
    </>
  );
};

//Styled Components

const Heading = styled.div`
  margin-top: 8rem;
  text-align: center;
`;

const ProductsContainer = styled.div`
  max-width: 1024px;
  width: 80%;
  margin: 70px auto 0;
  gap: 12px;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(230px, 1fr));
  grid-gap: 15px;
`;

export default Store;
