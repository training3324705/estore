import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import {
  MainWrapper,
  LoginCardWrapper,
  LoginHeading,
  LoginHeadingWrapper,
  SeparationLine,
  Form,
  UserInput,
  LoginButtn,
  Errors,
} from "../styles/pages/login";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object({
  Name: yup
    .string()
    .required()
    .matches(/^[a-zA-Z0-9_]+$/, "Name must be AlphaNumeric"),
  Password: yup
    .string()
    .required("Please Enter your password")
    .min(6, "Must be exactly six Character.")
    .max(6, "Must be exactly six Character"),
});

const Login = () => {
  const [getError, setError] = useState("");
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const navigate = useNavigate();

  const token = localStorage.getItem("userToken");
  useEffect(() => {
    if (token) {
      navigate("/allproducts");
    }
  });

  const onSubmit = ({ Name, Password }) => {
    axios({
      url: "https://fakestoreapi.com/auth/login",
      method: "POST",
      data: {
        username: Name,
        password: Password,
      },
    })
      .then((res) => {
        localStorage.setItem("userToken", res.data.token);
        !"userToken" ? navigate("/") : navigate("/allproducts");
      })
      .catch((err) => {
        setError(err.response.data);
      });
  };

  return (
    <MainWrapper>
      <LoginCardWrapper>
        <LoginHeadingWrapper>
          <LoginHeading>Login</LoginHeading>
        </LoginHeadingWrapper>
        <SeparationLine />
        <Form onSubmit={handleSubmit(onSubmit)}>
          <UserInput placeholder="UserName" {...register("Name")} />
          <Errors>{errors.Name?.message || getError}</Errors>
          <UserInput placeholder="UserPassword" {...register("Password")} />
          <Errors>{errors.Password?.message || getError}</Errors>
          <LoginButtn type="submit">Login</LoginButtn>
        </Form>
      </LoginCardWrapper>
    </MainWrapper>
  );
};

export default Login;
