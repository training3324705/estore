import Store from "./pages/Store";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Cart from "./pages/Cart";
import "./App.css";
import Login from "./pages/login";
import ProtectedRoutes from "./components/privateroute";
import Allpoducts from "./pages/allproducts";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route element={<ProtectedRoutes />}>
            <Route path="/allproducts" element={<Allpoducts />} />
            <Route path="/allproducts/:category" element={<Store />} />
            <Route exact path="/cart" element={<Cart />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
